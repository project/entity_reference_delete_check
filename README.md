# Entity Reference Delete Check

This module alters the `\Drupal\Core\Entity\ContentEntityDeleteForm`
to add information about existing references to the entity.

## Development

This project uses [DDEV Drupal Contrib](https://github.com/ddev/ddev-drupal-contrib) for local development.

Setup:
```shell
ddev start
ddev poser
ddev symlink-project
```

Start:
```shell
ddev start
```

Start Browser:
```shell
ddev launch
```

## Contributing

### Drupal Code Style

Check for issues with the Drupal code style:
```shell
ddev phpcs
```

Fix issues with the Drupal code style:
```shell
ddev phpcbf
```
