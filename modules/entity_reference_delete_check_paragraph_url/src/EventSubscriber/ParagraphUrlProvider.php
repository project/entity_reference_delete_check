<?php

namespace Drupal\entity_reference_delete_check_paragraph_url\EventSubscriber;

use Drupal\entity_reference_delete_check\Event\DeleteCheckEntityUrlEvent;
use Drupal\paragraphs\ParagraphInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * This event subscriber provides URLs to pages containing the paragraph entity.
 */
class ParagraphUrlProvider implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DeleteCheckEntityUrlEvent::class => 'setUrl',
    ];
  }

  /**
   * Set the URL for paragraph entities.
   */
  public function setUrl(DeleteCheckEntityUrlEvent $event): void {
    $entity = $event->entity;
    if (!$entity instanceof ParagraphInterface) {
      return;
    }
    while ($entity instanceof ParagraphInterface) {
      $entity = $entity->getParentEntity();
    }
    if (!$entity) {
      return;
    }
    try {
      $event->setUrl($entity->toUrl());
    }
    catch (\Exception) {
      // We can't get the URL this way.
    }
  }

}
