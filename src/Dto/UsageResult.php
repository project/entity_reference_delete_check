<?php

namespace Drupal\entity_reference_delete_check\Dto;

/**
 * This data transfer object includes information about entity usages.
 */
class UsageResult {

  /**
   * The usages of the entity in entity reference fields.
   *
   * @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface[]
   */
  public readonly array $entityReferenceFields;

  /**
   * Construct a usage result.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface[] $entity_reference_fields
   *   see {@see \Drupal\entity_reference_delete_check\Dto\UsageResult::$entityReferenceFields}.
   */
  public function __construct(array $entity_reference_fields) {
    $this->entityReferenceFields = $entity_reference_fields;
  }

  /**
   * Check whether usages exist or not.
   */
  public function isEmpty(): bool {
    return !$this->entityReferenceFields;
  }

}
