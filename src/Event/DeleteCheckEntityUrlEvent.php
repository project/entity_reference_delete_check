<?php

namespace Drupal\entity_reference_delete_check\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * This event encapsulates an entity and a URL to it.
 *
 * This is used so that a URL to the place where an entity is used
 * can be provided to the user during the delete check.
 */
class DeleteCheckEntityUrlEvent extends Event {

  /**
   * The entity.
   */
  public readonly EntityInterface $entity;

  /**
   * The URL to the entity.
   */
  private ?Url $url = NULL;

  /**
   * Construct a delete check entity url event.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Get the URL.
   */
  public function getUrl(): ?Url {
    return $this->url;
  }

  /**
   * Set the URL.
   */
  public function setUrl(?Url $url): void {
    $this->url = $url;
  }

}
