<?php

namespace Drupal\entity_reference_delete_check\EventSubscriber;

use Drupal\entity_reference_delete_check\Event\DeleteCheckEntityUrlEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * This event subscriber provides entity URLs for entities.
 */
class DeleteCheckEntityUrlEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DeleteCheckEntityUrlEvent::class => 'setUrl',
    ];
  }

  /**
   * Set the url of the entity.
   */
  public function setUrl(DeleteCheckEntityUrlEvent $event): void {
    $entity = $event->entity;
    try {
      $event->setUrl($entity->toUrl());
    }
    catch (\Exception) {
      // We can't get the entity url, let some other gentleman get it.
    }
  }

}
