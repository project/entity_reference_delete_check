<?php

namespace Drupal\entity_reference_delete_check\Service;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\entity_reference_delete_check\Dto\UsageResult;

/**
 * This class searches for existing references to an entity.
 */
class EntityReferenceUsageChecker {

  /**
   * The entity type manager.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type bundle info.
   */
  private EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The entity field manager.
   */
  private EntityFieldManagerInterface $entityFieldManager;

  /**
   * Construct an entity reference usage checker.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manger,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    EntityFieldManagerInterface $entity_field_manager,
  ) {
    $this->entityTypeManager = $entity_type_manger;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Check for usages of the content entity.
   */
  public function checkUsages(
    ContentEntityInterface $content_entity,
  ): UsageResult {
    return new UsageResult(
      $this->checkEntityReferenceFieldUsage($content_entity),
    );
  }

  /**
   * Check for entity reference field usages of the entity.
   *
   * @return \Drupal\Core\Field\EntityReferenceFieldItemListInterface[]
   *   see {@see \Drupal\entity_reference_delete_check\Dto\UsageResult::$entityReferenceFields}
   */
  private function checkEntityReferenceFieldUsage(
    ContentEntityInterface $content_entity,
  ): array {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface[] $entity_reference_fields */
    $entity_reference_fields = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
      if (!$entity_type->entityClassImplements(
        FieldableEntityInterface::class
      )) {
        continue;
      }
      $entity_type_id = $entity_type->id();
      foreach ($this->entityTypeBundleInfo->getBundleInfo(
        $entity_type_id,
      ) as $bundle_name => $bundle_info) {
        foreach ($this->entityFieldManager->getFieldDefinitions(
          $entity_type_id,
          $bundle_name,
        ) as $field_name => $field_definition) {
          if (
            $field_definition->getType() !== 'entity_reference'
            || $field_definition->isComputed()
            || $field_definition->getSetting('target_type') !== $content_entity->getEntityTypeId()
          ) {
            continue;
          }
          try {
            $entity_storage = $this->entityTypeManager->getStorage(
              $entity_type_id,
            );
          }
          catch (InvalidPluginDefinitionException | PluginNotFoundException) {
            continue;
          }
          $query = $entity_storage->getQuery()->accessCheck(TRUE);
          $query->condition($field_name, $content_entity->id());
          if ($bundle_name !== $entity_type_id) {
            $bundle_key = $entity_type->getKey('bundle');
            $query->condition($bundle_key, $bundle_name);
          }
          $result = $query->execute();
          $referencing_entities = $result ? $entity_storage->loadMultiple(
            $result
          ) : [];
          foreach ($referencing_entities as $referencing_entity) {
            $entity_reference_fields[] = $referencing_entity->get(
              $field_name
            );
          }
        }
      }
    }

    return $entity_reference_fields;
  }

}
